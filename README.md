# ambient-image

> ambient-image is a Vue component that can be used to add colored shadows to images. As shown on the demos below the shadow accepts the colors of the image creating beautiful and fully configurable ambience via handy props for blur, elevation, etc.

## Installation, configuration and usage

Check https://ambient-image.wemakesites.net for documentation and demos

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
